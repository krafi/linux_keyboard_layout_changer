import sys
from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QVBoxLayout, QHBoxLayout, QInputDialog
from subprocess import call

class LanguageSwitcher(QWidget):
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        self.languages = {'us': 'English', 'ru': 'Russian'}
        Height = 50
        weight = 300
        self.active_language = None

        self.create_language_buttons()
        self.create_add_layout_button()

        layout = QVBoxLayout()
        layout.setSpacing(1)
        #layout = QHBoxLayout() # horizontal

        layout.addWidget(self.add_layout_button)
        layout.addLayout(self.layout_buttons)
        self.setLayout(layout)

        self.setWindowTitle('Language Switcher')
        self.setGeometry(50, 20, weight, Height)

    def create_language_buttons(self):
        self.layout_buttons = QHBoxLayout()
        self.layout_buttons.setSpacing(1)
        self.layout_buttons.setContentsMargins(0, 0, 0, 0)

        for lang_code, lang_name in self.languages.items():
            button = QPushButton(f'({lang_code})')
            button.clicked.connect(lambda _, code=lang_code: self.switch_language(code))
            self.layout_buttons.addWidget(button)

    def create_add_layout_button(self):
        self.add_layout_button = QPushButton('Add Keyboard Layout')
        self.add_layout_button.clicked.connect(self.add_keyboard_layout)

    def switch_language(self, lang_code):
        call(['setxkbmap', lang_code])
        self.set_active_language(lang_code)

    def set_active_language(self, lang_code):
        if self.active_language:
            self.active_language.setStyleSheet('')
        for button_index in range(self.layout_buttons.count()):
            button = self.layout_buttons.itemAt(button_index).widget()
            if button.text().endswith(f'({lang_code})'):
                button.setStyleSheet('background-color: green;')
                self.active_language = button

    def add_keyboard_layout(self):
        layout_file = 'available_keyboard_layout.txt'
        try:
            with open(layout_file, 'r') as file:
                layouts = file.read().splitlines()
        except FileNotFoundError:
            print(f"File '{layout_file}' not found.")
            return

        layout, ok = QInputDialog.getItem(
            self, "Select Keyboard Layout", "Choose a layout:", layouts, 0, False
        )

        if ok:
            self.create_layout_button(layout)
            self.switch_language(layout)

    def create_layout_button(self, layout):
        layout_button = QPushButton(f'Switch to {layout}')
        layout_button.clicked.connect(lambda _, l=layout: self.switch_language(l))
        self.layout_buttons.addWidget(layout_button)

if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = LanguageSwitcher()
    window.show()
    sys.exit(app.exec_())
